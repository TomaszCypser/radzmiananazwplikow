﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadZmianaNazwPlikow
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            List<FileInfo> fileList = new List<FileInfo>();
            Console.WriteLine("Wciśnij dowolny klawisz, aby wybrać katalog do zmiany nazw plików");
            Console.ReadKey();
            string path;
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowDialog();
            path = fd.SelectedPath;
            if(path == "")
            {
                return;
            }
            DirectoryInfo directory = new DirectoryInfo(path);
            fileList = directory.GetFiles().OrderBy(p => p.CreationTime).ToList();
            WriteAllFilesName(directory);
            Console.WriteLine("Na pewno chcesz zmienić nazwy plików w tym katalogu? Jeśli tak to napisz \"Y\"");
            string input = Console.ReadLine();
            if(input.ToUpper() == "Y")
            {
                ChangeFilesName(directory);
            }
            else
            {
                Console.WriteLine("Nie zmieniono nazw plików");
                return;
            }
            WriteAllFilesName(directory);
            Console.ReadKey();
        }

        public static void ChangeFilesName(DirectoryInfo directory)
        {
            int fileNumber = 1;
            var fileList = directory.GetFiles().OrderBy(p => p.CreationTime).ToList();
            DirectoryInfo diTemp = Directory.CreateDirectory(directory.FullName + "\\temp\\");
            foreach (FileInfo file in fileList)
            {
                File.Move(file.FullName, (file.DirectoryName + "\\" + "temp" + "\\" + fileNumber.ToString() + file.Extension));
                fileNumber++;
            }
            var fileListTemp = diTemp.GetFiles().OrderBy(p => p.CreationTime).ToList();
            foreach (FileInfo file in fileListTemp)
            {
                File.Move(file.FullName, directory.FullName + "\\" + file.Name );
            }
            diTemp.Delete();
        }

        public static void WriteAllFilesName(DirectoryInfo directory)
        {
            Console.WriteLine("W katalogu znajdują się następujące pliki:");
            string[] fileList;
            fileList = Directory.GetFiles(directory.FullName);
            for (int i = 0; i < fileList.Length; i++)
            {
                Console.WriteLine(fileList[i] + " " + File.GetCreationTime(directory.FullName));
            }
            Console.WriteLine();
        }
    }
}
